GSPopoverWindow
===

The intention of GSPopoverWindow is to provide a method for displaying windows similar to UIPopoverWindows from iOS.  The window design is done in Xojo, then declares are used to make the appropriate transparencty/window shape (depending on operating system) and the popover background is drawn in the paint event.


**Version**

v1.0


**Contribute**

If you want to contribute to GSPopoverWindow, I'm happy to have you do it, but I've never worked with others and am trying to figure out how to best do that. I know it has to do with pull requests, so if you can help let me know. wbgookin at gmail dot com.

If you'd like to send me money (heh) because this is just what you needed, you can PayPal it to public at gookinsoftware dot com. Voluntary, of course.

**Notes**

This code was originally written by Bill Gookin. Contributors will be listed below.


**Xojo Version Used**

* Version 2013r3.3


**Repository Location**

* https://bitbucket.org/wbgookin/gspopoverwindow